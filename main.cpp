#include "ympmarket.h"

#include <QApplication>
#include <ymp.h>

int main(int argc, char *argv[])
{
    ymp_init(argv, argc);
    QApplication a(argc, argv);
    ympMarket w;
    w.show();
    return a.exec();
}

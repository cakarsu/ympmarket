#ifndef YMPMARKET_H
#define YMPMARKET_H

#include <QMainWindow>
#include <QApplication>
#include <QTreeWidget>
#include <QListWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class ympMarket; }
QT_END_NAMESPACE

class ympMarket : public QMainWindow
{
    Q_OBJECT

public:
    ympMarket(QWidget *parent = nullptr);
    ~ympMarket();
    QString paketAdi;
    QString paketGrup, kurulumTuru;
    QFont font;
    QIcon yukluSimge = QApplication::style()->standardIcon(QStyle::SP_DialogApplyButton);
    QIcon yukluDegilSimge = QApplication::style()->standardIcon(QStyle::SP_DialogCloseButton);

    void paketBilgisi(QTreeWidgetItem *item);

private slots:
    void on_treeWidget_itemClicked(QTreeWidgetItem *item);

    void on_yukle_btn_clicked();

    void on_kaldir_btn_clicked();

    void on_listWidgetPaket_itemClicked(QListWidgetItem *item);

    void grupListele(QString paketAdi);

    void paketleriListele();

    void depoPaketleriListele(QString depoAd);

    void on_kaynakRbtn_clicked();

    void on_ikiliRbtn_clicked();

    void on_actionQt_Hakk_triggered();

    void on_actionKapat_triggered();

    void on_actionKontrol_Et_triggered();

    void on_actionGuncelle_triggered();

    void on_lineEdit_textChanged(const QString &arg1);

    void on_actionDepolar_triggered();

private:
    Ui::ympMarket *ui;
};
#endif // YMPMARKET_H

#include "ympmarket.h"
#include "ui_ympmarket.h"
#include <QDebug>
#include <QFile>
#include <QProcess>
#include <QPushButton>
#include <QTextStream>
#include <QMessageBox>
#include <QCheckBox>
#include <ymp/ymp.h>
#include <QDir>



//YMP package info
void ympMarket::paketBilgisi(QTreeWidgetItem *item){
    ui->textEdit->clear();
    ui->kaldir_btn->setEnabled(false);
    ui->yukle_btn->setEnabled(false);
    ui->textEdit->setFontPointSize(12);
    paketAdi="";

    QString str1 = item ->text(1);//isim_text;
    QByteArray ba = str1.toLocal8Bit();
    const char *ad = ba.data();

    int len;

    package *p = get_package(ad);
    char* name= package_get(p, "name");
    char* vers= package_get(p, "version");
    char* desc= package_get(p, "description");
    char** grup= package_gets(p,"group",&len);
    char** depen= package_gets(p,"depends",&len);
    gboolean kurulumu = package_is_installed(p);
    //char* depo= package_get(p, "repository");
    //printf("%s - %s => %s\n", grup, name, desc);
    //qDebug()<< "Bağımlılık : " << *depen[0]<< " - " << *depen[1];
    paketAdi=name;

    ui->textEdit->append("Paket Adı \t: ");
        ui->textEdit->moveCursor(QTextCursor::End);
    ui->textEdit->insertPlainText(name);
    if(kurulumu==1){
        ui->textEdit->insertPlainText(" (Kurulu)");
        ui->kaldir_btn->setEnabled(true);
    }
    else{
        ui->textEdit->insertPlainText(" (Kurulu Değil)");
        ui->yukle_btn->setEnabled(true);
    }

    ui->textEdit->append("Açıklama \t: ");
    ui->textEdit->moveCursor(QTextCursor::End);
    ui->textEdit->insertPlainText(desc);

    ui->textEdit->append("Versiyon \t: ");
    ui->textEdit->moveCursor(QTextCursor::End);
    ui->textEdit->insertPlainText(vers);
    ui->textEdit->insertPlainText("\n");

    ui->textEdit->append("Bağımlılıkları : \n");
    ui->textEdit->moveCursor(QTextCursor::End);
    for(unsigned int i=0;depen[i]!=NULL;i++){
        ui->textEdit->insertPlainText("- ");
        ui->textEdit->insertPlainText(depen[i]);
        ui->textEdit->insertPlainText(" \t\n");
    }

    ui->textEdit->append("Grup \t: ");
    ui->textEdit->moveCursor(QTextCursor::End);
    ui->textEdit->insertPlainText(*grup);
    ui->textEdit->insertPlainText("\n");
}

//QTreeWidget package list
void ympMarket::grupListele(QString paketGrup){

    if(paketGrup=="Bütün Paketler"){
        paketleriListele();
    }

    ui->txtDepoAd->setText(paketGrup + " - grubu paketleri");

    int len;
    char** list = list_available_packages(&len);

    for(int i=0;list[i];i++){
        package *p = get_package(list[i]);
        char* name= package_get(p, "name");
        char* vers= package_get(p, "version");
        char* desc= package_get(p, "description");
        char** grup= package_gets(p,"group",&len);
        gboolean kurulumu = package_is_installed(p);
        if(paketGrup==*grup){
            QTreeWidgetItem *dene = new QTreeWidgetItem(ui->treeWidget);
            if(kurulumu==1){
                dene->setIcon(0,yukluSimge);  //setCheckState(0,Qt::Checked);
                package *p1 = get_installed_package(list[i]);
                //char* name1= package_get(p1, "name");
                char* vers1= package_get(p1, "version");
                dene->setText(2,vers1);
            }
            else{
                dene->setIcon(0,yukluDegilSimge);
            }

            dene->setText(1,name);
            dene->setText(3,vers);
            dene->setText(4,desc);
            ui->treeWidget->addTopLevelItem(dene);
        }

    }

}

//QTreeWidget group list
void ympMarket::paketleriListele(){

    QStringList etiket;
    etiket << "Kurulu" << "Paket Adı" << "Yüklü Versiyon" << "Versiyon" << "Açıklama"; //treeWidget sütun başlıkları
    ui->treeWidget->setHeaderLabels(etiket);
    ui->treeWidget->setColumnWidth(0,60); //1. sütün genişliği
    ui->treeWidget->setColumnWidth(1,200); //2. sütün genişliği
    ui->treeWidget->setColumnWidth(2,120); //3. sütün genişliği
    ui->treeWidget->setColumnWidth(3,120); //3. sütün genişliği
    ui->listWidgetPaket->clear();

    ui->txtDepoAd->setText("Bütün Paketler");

    QList<QString> grupListe;
    grupListe << "Bütün Paketler" << "Ana Depo" << "Extra Depo";
    int len;

    if(QDir("/var/lib/ymp/index").exists()){
       ui->textEdit->setText("");
    }
    else {
        update_repo();
    }

    //char** list = list_installed_packages(&len);
    char** list = list_available_packages(&len);

    for(int i=0;list[i];i++){
        package *p = get_package(list[i]);

        char* name= package_get(p, "name");
        char* vers= package_get(p, "version");
        char* desc= package_get(p, "description");
        char** grup= package_gets(p,"group",&len);
        gboolean kurulumu = package_is_installed(p);
        //qDebug()<<p1->version;
        //printf("%s - %s => %s\n", grup, name, desc);
        //qDebug()<< *grup;

        QTreeWidgetItem *dene = new QTreeWidgetItem(ui->treeWidget);
        if(kurulumu==1){
            dene->setIcon(0,yukluSimge);  //setCheckState(0,Qt::Checked);// setText(0,"K");

            package *p1 = get_installed_package(list[i]);
            //char* name1= package_get(p1, "name");
            char* vers1= package_get(p1, "version");
            dene->setText(2,vers1);
        }
        else{
            dene->setIcon(0,yukluDegilSimge);
        }

        dene->setText(1,name);
        dene->setText(3,vers);
        dene->setText(4,desc);
        ui->treeWidget->addTopLevelItem(dene);

        if(*grup==NULL){
            ui->textEdit->append(name);
            ui->textEdit->append(*grup);
        }
        grupListe <<  *grup;
    }

    font.setPointSize(12);
    ui->treeWidget->setFont(font);
    ui->treeWidget->setSortingEnabled(true);
    ui->treeWidget->sortByColumn(1,Qt::AscendingOrder);

    grupListe.removeDuplicates();

    grupListe.sort();

    ui->listWidgetPaket->setFont(font);
    ui->listWidgetPaket->addItems(grupListe);

    //Deneme

    /*int len1;
    char** liste = list_installed_packages(&len1);
    for (int i=0;liste[i];i++) {
        package *p1 = get_installed_package(liste[i]);
        char* namei= package_get(p1, "name");
        ui->textEdit->append(namei);
        ui->textEdit->moveCursor(QTextCursor::End);
        ui->textEdit->insertPlainText(" -> ");
        ui->textEdit->moveCursor(QTextCursor::End);
        ui->textEdit->insertPlainText(p1->version);
    }*/

}

// Repo package list
void ympMarket::depoPaketleriListele(QString depoAd){

    int len1=0;
    repository **r = get_repos(&len1);

    QString depoYaz;
    if(depoAd=="main"){depoYaz="Ana Depo";}
    if(depoAd=="extra"){depoYaz="Extra Depo";}

    ui->txtDepoAd->setText(depoYaz + " Paketleri");

    for(int j=0;r[j];j++){
        if(r[j]->name==depoAd){
            int k=0;
            char** pkgs = repository_list_packages(r[j],&k);

            int len;
            char** list = list_available_packages(&len);

            for(int l=0;pkgs[l];l++){
                //printf("%s => %s \n", pkgs[l], r[j]->name);
                QTreeWidgetItem *dene = new QTreeWidgetItem(ui->treeWidget);

                package *p = get_package(list[l]);

                char* name= package_get(p, "name");
                char* vers= package_get(p, "version");
                char* desc= package_get(p,"description");
                gboolean kurulumu = package_is_installed(p);

                if(kurulumu==1){
                    dene->setIcon(0,yukluSimge);  //setCheckState(0,Qt::Checked);
                    package *p1 = get_installed_package(list[l]);
                    //char* name1= package_get(p1, "name");
                    char* versy= package_get(p1, "version");
                    dene->setText(2,versy);
                }
                else{
                    dene->setIcon(0,yukluDegilSimge);
                }

                dene->setText(1,name);
                dene->setText(3,vers);
                dene->setText(4,desc);
            }
        }

    }

}

ympMarket::ympMarket(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::ympMarket)
{
    ui->setupUi(this);

    paketleriListele();
    kurulumTuru="ikili";
}

ympMarket::~ympMarket()
{
    delete ui;
}

//onclick package info
void ympMarket::on_treeWidget_itemClicked(QTreeWidgetItem *item)
{
    paketBilgisi(item);
}

//install package
void ympMarket::on_yukle_btn_clicked()
{
    QProcess yukle;
    QStringList anahtar;

    if(kurulumTuru=="ikili"){
        anahtar << "it" << paketAdi << "--no-color" << "--ignore-quarantine" <<"--no-emerge";
    }
    else{
        anahtar << "it" << paketAdi << "--no-color" << "--ignore-quarantine";
    }


    yukle.start("ymp",anahtar,QIODevice::ReadWrite);
    yukle.waitForFinished();

    QString paket_bilgi = yukle.readAllStandardError();
    ui->textEdit->clear();
    ui->textEdit->append(paket_bilgi);
    qDebug()<<paketAdi << " - "<< paket_bilgi;
    ui->textEdit->append(paketAdi +" kuruldu.");
    QMessageBox::information(this, "Bilgi", paketAdi + " Paketi\n" + paket_bilgi);

    //ui->textEdit->clear();
}

//remove package
void ympMarket::on_kaldir_btn_clicked()
{
    QProcess yukle;
    QStringList anahtar;
    anahtar << "remove" << paketAdi <<"--no-color" <<"--ignore-dependency";


    yukle.start("ymp",anahtar,QIODevice::ReadWrite);
    yukle.waitForFinished();
    QString paket_bilgi = yukle.readAllStandardError();
    ui->textEdit->clear();
    ui->textEdit->append(paket_bilgi);
    //ui->textEdit->append(paketAdi + " kaldırıldı.");
    QMessageBox::information(this, "Bilgi", paketAdi + " Paketi\n" + paket_bilgi);

    //ui->textEdit->clear();
}

//list package
void ympMarket::on_listWidgetPaket_itemClicked(QListWidgetItem *item)
{
    ui->treeWidget->clear();
    ui->textEdit->clear();

    /*QModelIndex paket_isim = ui->listWidgetPaket->currentIndex(); //indexFromItem(item);
    QVariant paket_data = ui->listWidgetPaket->model()->data(paket_isim);
    QString paket_text = paket_data.toString();*/
    //qDebug()<< item->text();


    if(item->text()=="Bütün Paketler"){
        paketleriListele();
    }
    else if(item->text()=="Ana Depo"){
        depoPaketleriListele("main");
    }
    else if(item->text()=="Extra Depo"){
        depoPaketleriListele("extra");
    }
    else{
        grupListele(item->text());
    }
}

//install type
void ympMarket::on_kaynakRbtn_clicked()
{
    kurulumTuru="kaynak";
}

//install type
void ympMarket::on_ikiliRbtn_clicked()
{
    kurulumTuru="ikili";
}


void ympMarket::on_actionQt_Hakk_triggered()
{
    QApplication::aboutQt();
}


void ympMarket::on_actionKapat_triggered()
{
    QApplication::quit();
}

//repo update
void ympMarket::on_actionKontrol_Et_triggered()
{

    try {
        update_repo();
        int len;
        ui->textEdit->setText("Güncellenecek Paketler");
        ui->textEdit->append("---------------------------------------------------------");
        char **paketler = get_upgradable_packages(&len);
        for(int j=0;paketler[j];j++){
            //qDebug()<<paketler[j]<<"\n";
            ui->textEdit->append(paketler[j]);
        }
    } catch (const std::exception& ex) {
        //QMessageBox::warning(this, "Hata!", paketAdi + ex.what() + " Güncellemeler alınamadı.");
        ui->textEdit->setText(ex.what());
    }

}

//upgrade packages
void ympMarket::on_actionGuncelle_triggered()
{

    QProcess guncelle;
    QStringList anahtar;
    anahtar << "it" << "--upgrade";

    guncelle.start("ymp",anahtar,QIODevice::ReadWrite);
    guncelle.waitForFinished();
    QString durum = guncelle.readAllStandardOutput();
    //qDebug()<<durum;
    ui->textEdit->clear();
    ui->textEdit->append("Güncelleme");
    ui->textEdit->append(durum);
    //QMessageBox::information(this, "Bilgi",durum);

}

//package search
void ympMarket::on_lineEdit_textChanged(const QString &arg1)
{
    QList<QTreeWidgetItem*> clist = ui->treeWidget->findItems(arg1,Qt::MatchWildcard | Qt::MatchRecursive,1);
    for (QTreeWidgetItem* item : clist) {
        //item->setBackground(1,Qt::red);
        ui->treeWidget->setCurrentItem(item,1);
        paketBilgisi(item);
    }

}

//show repo info
void ympMarket::on_actionDepolar_triggered()
{
    int len=0;
    repository **r = get_repos(&len);
    for(int j=0;r[j];j++){
        qDebug()<<"Depo =>"<< j <<" : " <<r[j]->name << r[j]->address <<"\n";
        ui->textEdit->append("Depo adı\t : ");
        ui->textEdit->moveCursor(QTextCursor::End);
        ui->textEdit->insertPlainText(r[j]->name);
        ui->textEdit->append("Adres\t : ");
        ui->textEdit->moveCursor(QTextCursor::End);
        ui->textEdit->insertPlainText(r[j]->address);
        ui->textEdit->append("\n");
    }

}

